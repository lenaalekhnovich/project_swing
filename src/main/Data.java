package main;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Босс on 21.06.2017.
 */
public class Data {

    private int id = 0;
    private Date date;
    private int idWeather = 1;
    private String cloud = "";
    private int temperature = 0;
    boolean isSelected = false;

    public Data(Date date, String cloud, int temperature) {
        this.date = date;
        this.cloud = cloud;
        this.temperature = temperature;
    }

    public Data(){
        date = new Date();
    }

    public Data(Data data){
        this.id = data.id;
        this.date = data.date;
        this.idWeather = data.idWeather;
        this.cloud = data.cloud;
        this.temperature = data.temperature;
        this.isSelected = data.isSelected;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getIdWeather() {
        return idWeather;
    }

    public void setIdWeather(int idWeather) {
        this.idWeather = idWeather;
    }

    public String getCloud() {
        return cloud;
    }

    public void setCloud(String cloud) {
        this.cloud = cloud;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public String toString() {
        String weather = idWeather == 1 ? "летная" : "нелетная";
        weather+= " погода";
        SimpleDateFormat format1 = new SimpleDateFormat("HH:mm dd-MM-yyyy");
        String dateStr = format1.format(date);
        return id + ". " + dateStr + "; " + weather + "; " + cloud + "; " + temperature + "\n";
    }
}
