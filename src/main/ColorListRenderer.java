package main;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Босс on 21.06.2017.
 */
public class ColorListRenderer extends DefaultListCellRenderer {

    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean hasFocus){

        JLabel label = (JLabel)super.getListCellRendererComponent(list, value, index,   isSelected, hasFocus);
        label.setBackground((Color)value);
        label.setText(" ");
        return label;
    }

}
