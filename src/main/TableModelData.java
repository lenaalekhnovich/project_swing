package main;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Босс on 21.06.2017.
 */
public class TableModelData implements TableModel {

    private Set<TableModelListener> listeners = new HashSet<TableModelListener>();

     private static List<Data> dataList;

    public TableModelData(List<Data> dataList) {
        TableModelData.dataList = new LinkedList<>();
        for(Data data:dataList){
            Data newData = new Data(data);
            TableModelData.dataList.add(newData);
        }
    }

    public void addTableModelListener(TableModelListener listener) {
        listeners.add(listener);
    }

    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return Boolean.class;
            case 1:
                return Integer.class;
            case 2:
                return Object.class;
            case 3:
                return Integer.class;
            case 4:
                return String.class;
            case 5:
                return String.class;
            case 6:
                return String.class;
            case 7:
                return Integer.class;
        }
        return String.class;
    }

    public int getColumnCount() {
        return 8;
    }

    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "";
            case 1:
                return "Номер записи";
            case 2:
                return "Дата наблюдения";
            case 3:
                return "id";
            case 4:
                return "Метеорологические\n условия";
            case 5:
                return "";
            case 6:
                return "Облачность";
            case 7:
                return "Температура";
        }
        return "";
    }

    public int getRowCount() {
        return dataList.size();
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        Data data = new Data();
        if(dataList.size() != 0 ) {
            data = dataList.get(rowIndex);
            data.setId(rowIndex + 1);
        }
        switch (columnIndex) {
            case 0:
                return data.isSelected();
            case 1:
                return data.getId();
            case 2:
                SimpleDateFormat format1 = new SimpleDateFormat("HH:mm dd-MM-yyyy");
                return format1.format(data.getDate());
            case 3:
                return data.getIdWeather();
            case 4:
                return "";
            case 5:
                return "";
            case 6:
                return data.getCloud();
            case 7:
                return data.getTemperature();
        }
        return "";
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return !(columnIndex == 1 || columnIndex == 3 || columnIndex == 4);
    }

    public void removeTableModelListener(TableModelListener listener) {
        listeners.remove(listener);
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                dataList.get(rowIndex).setSelected((Boolean)value);
                break;
            case 1:
                dataList.get(rowIndex).setId((int)value);
                break;
            case 2:
                dataList.get(rowIndex).setDate((Date)value);
                break;
            case 3:
                dataList.get(rowIndex).setIdWeather((int)value);
                break;
            case 4:
                break;
            case 5:
                break;
            case 6:
                dataList.get(rowIndex).setCloud((String)value);
                break;
            case 7:
                dataList.get(rowIndex).setTemperature((int)value);
                break;
        }
    }

    public List<Data> getDataList() {
        return dataList;
    }

    public static void setDataList(List<Data> dataList) {
        TableModelData.dataList = new LinkedList<>();
        for(Data data:dataList){
            Data newData = new Data(data);
            TableModelData.dataList.add(newData);
        }
    }

    public void setData(int index, Data data){
        dataList.set(index, data);
    }

    public void addData(Data data){
        dataList.add(data);
    }

    public void deleteData(int index){
        dataList.remove(index);
    }
}
