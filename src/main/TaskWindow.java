package main;

import sun.util.calendar.JulianCalendar;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

/**
 * Created by Босс on 21.06.2017.
 */
public class TaskWindow extends JFrame {

    private static List<Data> listAerodromeFirst;
    private static List<Data> listAerodromeSecond;
    private static List<Data> listAerodromeThird;
    private int checkAerodrome = 1;

    private JPanel mainPanel;
    private JPanel aerodromButtonPanel;
    private JPanel dataPanel;
    private JPanel infoPanel;
    private JPanel buttonPanel;
    private JButton aerodromFirstButton;
    private JButton aerodromSecondButton;
    private JButton aerodromThirdButton;
    private JButton createButton;
    private JButton saveButton;
    private JButton deleteButton;
    private JButton printButton;
    private JTextPane textPane;
    private JScrollPane scrollPane;
    private JTable table;
    private TableModelData model;
    private JComboBox comboBox;

    public static void setListAerodromeFirst(List<Data> dataList) {
        TaskWindow.listAerodromeFirst = dataList;
    }

    public static void setListAerodromeSecond(List<Data> listAerodromeSecond) {
        TaskWindow.listAerodromeSecond = listAerodromeSecond;
    }

    public static void setListAerodromeThird(List<Data> listAerodromeThird) {
        TaskWindow.listAerodromeThird = listAerodromeThird;
    }

    public TaskWindow() {
        super();
        $$$setupUI$$$();
        this.setContentPane(mainPanel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        table.setRowHeight(25);
        table.getTableHeader().setPreferredSize(new Dimension(2200, 32));
        TableColumnModel columnModel = table.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(20);
        columnModel.getColumn(3).setPreferredWidth(20);
        columnModel.getColumn(2).setPreferredWidth(100);
        columnModel.getColumn(4).setPreferredWidth(170);
        columnModel.getColumn(5).setPreferredWidth(20);

        Object[] data_property = {Color.green, Color.red};
        comboBox = new JComboBox(data_property);
        DefaultCellEditor editor = new DefaultCellEditor(comboBox);
        DateCellEditor dateCellEditor = new DateCellEditor();
        columnModel.getColumn(2).setCellEditor(dateCellEditor);
        columnModel.getColumn(5).setCellEditor(editor);
        columnModel.getColumn(0).setCellEditor(table.getDefaultEditor(Boolean.class));
        ColorTableCellRenderer renderer = new ColorTableCellRenderer();
        columnModel.getColumn(4).setCellRenderer(renderer);
        columnModel.getColumn(5).setCellRenderer(renderer);
        comboBox.setRenderer(new ColorListRenderer());

        columnModel.getColumn(0).setHeaderRenderer(new CheckBoxHeader(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                Object source = e.getSource();
                if (source instanceof AbstractButton == false) return;
                boolean checked = e.getStateChange() == ItemEvent.SELECTED;
                for (int x = 0, y = table.getRowCount(); x < y; x++) {
                    table.setValueAt(new Boolean(checked), x, 0);
                }
            }
        }));
        pack();
        setVisible(true);
        initListeners();
    }

    private void initListeners() {
       /* comboBox.addItemListener(e -> {
            int colorInt = comboBox.getSelectedIndex();
            int index = table.getSelectedRow();
            Data data = model.getDataList().get(index);
            data.setIdWeather(++colorInt);
            model.setData(index, data);
            //ColorTableCellRenderer renderer = new ColorTableCellRenderer();
            //renderer.repaint(table);
            //table.getColumnModel().getColumn(4).setCellRenderer(renderer);
            //table.getColumnModel().getColumn(5).setCellRenderer(renderer);
            //table.updateUI();
        });*/
        comboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int colorInt = comboBox.getSelectedIndex();
                int index = table.getSelectedRow();
                Data data = model.getDataList().get(index);
                data.setIdWeather(++colorInt);
                model.setData(index, data);
                table.updateUI();
            }
        });
        aerodromFirstButton.addActionListener(e -> {
            for (Data data : listAerodromeFirst) {
                data.setSelected(false);
            }
            model.setDataList(listAerodromeFirst);
            checkAerodrome = 1;
            textPane.setText("");
            table.updateUI();
        });
        aerodromSecondButton.addActionListener(e -> {
            for (Data data : listAerodromeSecond) {
                data.setSelected(false);
            }
            model.setDataList(listAerodromeSecond);
            checkAerodrome = 2;
            textPane.setText("");
            table.updateUI();
        });
        aerodromThirdButton.addActionListener(e -> {
            for (Data data : listAerodromeThird) {
                data.setSelected(false);
            }
            model.setDataList(listAerodromeThird);
            checkAerodrome = 3;
            textPane.setText("");
            table.updateUI();
        });
        createButton.addActionListener(e -> {
            model.addData(new Data());
            table.updateUI();
        });
        deleteButton.addActionListener(e -> {
            List<Data> list = model.getDataList();
            for (int i = list.size() - 1; i >= 0; i--) {
                Data data = list.get(i);
                if (data.isSelected) {
                    model.deleteData(i);
                }
            }
            table.updateUI();
        });
        saveButton.addActionListener(e -> {
            switch (checkAerodrome) {
                case 1:
                    listAerodromeFirst = model.getDataList();
                    break;
                case 2:
                    listAerodromeSecond = model.getDataList();
                    break;
                case 3:
                    listAerodromeThird = model.getDataList();
                    break;
            }
        });
        printButton.addActionListener(e -> {
            List<Data> list = null;
            String text = "Аэродром " + checkAerodrome + ":\n";
            /*switch (checkAerodrome) {
                case 1:
                    list = listAerodromeFirst;
                    break;
                case 2:
                    list = listAerodromeSecond;
                    break;
                case 3:
                    list = listAerodromeThird;
                    break;
            }
            for (int i = 0; i < list.size(); i++) {
                Data data = list.get(i);
                data.setSelected(model.getDataList().get(i).isSelected);
                data.setId(model.getDataList().get(i).getId());
                if (data.isSelected) {
                    text += data.toString();
                }
            }*/
            for (Data data : model.getDataList()) {
                if (data.isSelected) {
                    text += data.toString();
                }
            }
            textPane.setText(text);
        });
    }

    private void createUIComponents() {
        mainPanel = new JPanel();
        aerodromThirdButton = new JButton();
        createButton = new JButton();
        saveButton = new JButton();
        deleteButton = new JButton();
        printButton = new JButton();
        model = new TableModelData(listAerodromeFirst);
        table = new JTable(model);
        scrollPane = new JScrollPane(table);
        aerodromButtonPanel = new JPanel();
        buttonPanel = new JPanel();
        dataPanel = new JPanel();
        infoPanel = new JPanel();
    }

    public static void main(String[] args) {
        List<Data> list = new LinkedList<>();
        list.add(new Data(new Date(), "1/10", 20));
        list.add(new Data(new Date(), "2/10", 28));
        list.add(new Data(new Date(), "3/10", 22));
        TaskWindow.setListAerodromeFirst(list);
        list = new LinkedList<>();
        list.add(new Data(new Date(), "4/10", 23));
        list.add(new Data(new Date(), "5/10", 22));
        list.add(new Data(new Date(), "6/10", 19));
        TaskWindow.setListAerodromeSecond(list);
        list = new LinkedList<>();
        list.add(new Data(new Date(), "7/10", 25));
        list.add(new Data(new Date(), "8/10", 26));
        list.add(new Data(new Date(), "9/10", 22));
        TaskWindow.setListAerodromeThird(list);
        new TaskWindow();
    }


    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        createUIComponents();
        mainPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 3, new Insets(0, 0, 0, 0), -1, -1));
        aerodromButtonPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(3, 1, new Insets(0, 0, 420, 0), -1, -1));
        mainPanel.add(aerodromButtonPanel, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 2, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        aerodromButtonPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createRaisedBevelBorder(), null));
        aerodromFirstButton = new JButton();
        aerodromFirstButton.setFont(new Font(aerodromFirstButton.getFont().getName(), aerodromFirstButton.getFont().getStyle(), 10));
        aerodromFirstButton.setText("Аэродром 1");
        aerodromFirstButton.setVerticalAlignment(1);
        aerodromButtonPanel.add(aerodromFirstButton, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_NORTH, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        aerodromSecondButton = new JButton();
        aerodromSecondButton.setFont(new Font(aerodromSecondButton.getFont().getName(), aerodromSecondButton.getFont().getStyle(), 10));
        aerodromSecondButton.setText("Аэродром 2");
        aerodromSecondButton.setVerticalAlignment(1);
        aerodromSecondButton.setVerticalTextPosition(0);
        aerodromButtonPanel.add(aerodromSecondButton, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_NORTH, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        aerodromThirdButton.setFont(new Font(aerodromThirdButton.getFont().getName(), aerodromThirdButton.getFont().getStyle(), 10));
        aerodromThirdButton.setText("Аэродром 3");
        aerodromButtonPanel.add(aerodromThirdButton, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        dataPanel.setLayout(new BorderLayout(0, 0));
        mainPanel.add(dataPanel, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 2, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(800, -1), null, 0, false));
        dataPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createRaisedBevelBorder(), null));
        buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
        dataPanel.add(buttonPanel, BorderLayout.NORTH);
        createButton.setFont(new Font(createButton.getFont().getName(), createButton.getFont().getStyle(), 10));
        createButton.setText("Новая запись");
        buttonPanel.add(createButton);
        saveButton.setFont(new Font(saveButton.getFont().getName(), saveButton.getFont().getStyle(), 10));
        saveButton.setText("Сохранить");
        buttonPanel.add(saveButton);
        deleteButton.setFont(new Font(deleteButton.getFont().getName(), deleteButton.getFont().getStyle(), 10));
        deleteButton.setText("Удалить");
        buttonPanel.add(deleteButton);
        printButton.setFont(new Font(printButton.getFont().getName(), printButton.getFont().getStyle(), 10));
        printButton.setHorizontalAlignment(2);
        printButton.setText("Печать");
        buttonPanel.add(printButton);
        dataPanel.add(scrollPane, BorderLayout.CENTER);
        table.setAutoCreateRowSorter(true);
        table.setAutoResizeMode(2);
        table.setDropMode(DropMode.USE_SELECTION);
        table.setShowHorizontalLines(true);
        table.setSurrendersFocusOnKeystroke(false);
        table.setToolTipText("");
        table.setUpdateSelectionOnSort(true);
        table.putClientProperty("JTable.autoStartsEdit", Boolean.FALSE);
        table.putClientProperty("terminateEditOnFocusLost", Boolean.FALSE);
        scrollPane.setViewportView(table);
        infoPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.add(infoPanel, new com.intellij.uiDesigner.core.GridConstraints(0, 2, 2, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, new Dimension(300, -1), null, new Dimension(400, -1), 0, false));
        infoPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createRaisedBevelBorder(), null));
        textPane = new JTextPane();
        textPane.setText("");
        infoPanel.add(textPane, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(150, 50), null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return mainPanel;
    }
}
