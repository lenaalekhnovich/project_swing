package main;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

/**
 * Created by Босс on 15.06.2017.
 */
public class ColorTableCellRenderer extends DefaultTableCellRenderer {


    public Component getTableCellRendererComponent
            (JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component component = super.getTableCellRendererComponent
                (table, value, isSelected, hasFocus, row, column);
        setHorizontalAlignment(SwingConstants.CENTER);
        int checkColor = Integer.parseInt(table.getValueAt(row, 3).toString());
        Color color = Color.GREEN;
        switch (checkColor) {
            case 1:
                color = Color.GREEN;
                break;
            case 2:
                color = Color.RED;
                break;
        }
        table.getModel().setValueAt(checkColor, row, 4);
        component.setBackground(color);
        return component;
    }

    public void repaint(JTable table){
        this.getTableCellRendererComponent(table, null, true, true, 0, 0);
    }
}
